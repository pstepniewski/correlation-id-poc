### Project
Project use MDC concept [1] to implements correlationId pattern [2]. It covers three main cases:
- obtain correlationId for every request,
- propagate correlationId value in asynchronous system,
- add correlationId value in header of sending requests.

Project also contains proposition of logback.xml file. Log entries color depends on entry level
- trace, debug - white
- info - blue
- warn, error - red (error is bolded)

Recommended way to open log files:
`less -R messageService.log | grep INFO | cut -d\| -f1,5,8 | less -R`

##### run
`sbt run`

##### example request
`controllers/api.http`

### Problem

Mapped Diagnostic Context (MDC) is used to pass variables to logger in easy way. Unfortunately this solution does not
work in asynchronous systems. MDC is kept in ThreadLocal variable which isn`t propagated between Threads. The problem
is find way to pass request-context information to log entries in asynchronous systems.

### Research

1. Pass context using implicits [3]
2. Custom Akka Dispatcher [4, 5]
3. Custom Execution Context [4, 6]

##### 1. Pass context using implicits
The first solution not use MDC concept and depends on passing variables by adding to every method an implicit argument
`play.api.MarkerContext`. If you forget to do it your logs will not contain context values. Nothing will remind you
about adding implicit argument. Additionally new implicit argument obscures your code.

##### 2. Extend akka.dispatch.Dispatcher
Second solution is to extend the akka.dispatch.Dispatcher class and override `def prepare(): ExecutionContext` method.
The method is currently deprecated with comment
```text
     This method should no longer be overridden or called. It was
     originally expected that `prepare` would be called by
     all libraries that consume ExecutionContexts, in order to
     capture thread local context. However, this usage has proven
     difficult to implement in practice and instead it is
     now better to avoid using `prepare` entirely.
```
and will be removed in 2.12.0 akka version.

##### 3. Wrap ExecutionContext
The third solution extends ExecutionContext and pass calls to wrapped default ExecutionContext (which in Play
Framework is akka.dispatch.Dispatcher). Solution described in [4] is based on Play Framework before Dependency Injection
was introduced. So it should be adopted to current (2.6.x) version of Play.

### Solution description
- CorrelationId is obtained in CorrelationIdAction.
- Propagation of MDC: for every Future call is created dedicated context object (MdcExecutionContext). It get MDC state
from current thread and propagate it to destination thread.
- CorrelationId is added to header with WsClient.

##### MdcExecutionContexts
Extends ExecutionContext trait and overrides `def execute(runnable: Runnable): Unit` method. Implementation wraps Runnable
argument in `Runnable with OnCompleteRunnable` object. Inside `def run()` method of Runnable class it copy MDC context to
destination thread. Extends the OnCompleteRunnable trait is recommended because akka.dispatch.Dispatcher
uses implementation of execute method from akka.dispatch.BatchingExecutor trait which batch nested Future calls.
If use use only Runnable trait this optimization will be disabled. From scala.concurrent.OnCompleteRunnable documentation:
```text
    A marker indicating that a `java.lang.Runnable` provided to `scala.concurrent.ExecutionContext`
    wraps a callback provided to `Future.onComplete`.
    All callbacks provided to a `Future` end up going through `onComplete`, so this allows an
    ExecutionContext` to special-case callbacks that were executed by `Future` if desired.
```
To propagate MDC you need to inject `MdcExecutionContextProvider` and import `import mdcExecutionContextProvider.Implicits.get`

```scala
import javax.inject.{Inject, Singleton}
import infrastucture.MdcExecutionContextProvider

@Singleton
class CorrelationIdPocSimulator @Inject()(mdcExecutionContextProvider: MdcExecutionContextProvider) {

  import mdcExecutionContextProvider.Implicits.get

  //some code
}
```

##### CorrelationIdAction
Uses MdcExecutionContext. It generate new correlationId or take it from "X-Correlation-ID" header. CorrelationId value
is set in MDC and propagate using MdcExecutionContext.

### Similar solution
Implementations of correlationId pattern using monix: [7, 8]

### Sources

[1] https://www.slf4j.org/manual.html

[2] https://blog.rapid7.com/2016/12/23/the-value-of-correlation-ids/

[3] https://www.playframework.com/documentation/2.6.x/ScalaLogging

[4] http://yanns.github.io/blog/2014/05/04/slf4j-mapped-diagnostic-context-mdc-with-play-framework/

[5] https://github.com/jroper/thread-local-context-propagation/

[6] http://stevenskelton.ca/threadlocal-variables-scala-futures/

[7] https://olegpy.com/better-logging-monix-1/

[8] https://blog.softwaremill.com/correlation-ids-in-scala-using-monix-3aa11783db81
