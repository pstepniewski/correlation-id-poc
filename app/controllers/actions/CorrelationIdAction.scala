package controllers.actions

import controllers.actions.CorrelationIdAction._
import infrastucture.MdcExecutionContextProvider
import javax.inject.{Inject, Singleton}
import org.slf4j.MDC
import play.api.mvc.{ActionFilter, Request, Result}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

@Singleton
class CorrelationIdAction @Inject()(mdcExecutionContextProvider: MdcExecutionContextProvider)
  extends ActionFilter[Request] {

  override protected def filter[A](request: Request[A]): Future[Option[Result]] = {
    val correlationId = request.headers.get(X_CORRELATION_ID_HEADER) match {
      case Some(cId) => cId
      case None      => Random.alphanumeric.take(8).mkString
    }

    MDC.put("correlationId", correlationId)
    Future.successful(None)
  }


  override protected def executionContext: ExecutionContext = mdcExecutionContextProvider.get

}

object CorrelationIdAction {
  val X_CORRELATION_ID_HEADER = "X-Correlation-ID"
}
