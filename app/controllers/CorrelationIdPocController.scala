package controllers

import controllers.CorrelationIdPocController._
import controllers.actions.CorrelationIdAction
import infrastucture.{CorrelationIdPocSimulator, MdcExecutionContextProvider}
import javax.inject.{Inject, Singleton}
import play.api.mvc._

import scala.concurrent.Future
import scala.util.{Failure, Try}

@Singleton
class CorrelationIdPocController @Inject()(simulator: CorrelationIdPocSimulator,
                                           correlationIdAction: CorrelationIdAction,
                                           cc: ControllerComponents)
                                          (mdcExecutionContextProvider: MdcExecutionContextProvider)
  extends AbstractController(cc) {

  def simulate(): Action[AnyContent] = (Action andThen correlationIdAction).async { implicit request =>
    log.info(s"Processing new request. Testing look of logger levels.")
    log.trace("Trace level.")
    log.debug("Debug level.")
    log.info("Info level.")
    log.warn("Warn level.")
    Try { 5/0 } match {case Failure(e) => log.error("Error level", new RuntimeException("Exception wrapper", e))}

    simulator.simulate(controllers.routes.CorrelationIdPocController.simulateExternalSystem().absoluteURL())
    Future.successful(Ok)
  }


  def simulateExternalSystem(): Action[AnyContent] = (Action andThen correlationIdAction).async { implicit request =>
    log.info(s"External system call!")
    Future.successful(Ok)
  }
}

object CorrelationIdPocController {

  val log = play.api.Logger(getClass)
}
