package infrastucture

import akka.actor.ActorSystem
import infrastucture.CorrelationIdPocSimulator._
import javax.inject.{Inject, Singleton}
import org.slf4j.MDC
import play.api.libs.concurrent.CustomExecutionContext
import play.api.libs.ws.WSClient

import scala.concurrent.Future

@Singleton
class CorrelationIdPocSimulator @Inject()(system: ActorSystem, wsClient: WSClient)
                                         (mdcExecutionContextProvider: MdcExecutionContextProvider) {

  import mdcExecutionContextProvider.Implicits.get


  @Override
  def simulate(externalUrl: String): Unit = {
    val customEc = new MdcExecutionContext(MDC.getCopyOfContextMap, new CustomExecutionContext(system, "custom-ec"){})

    log.info(s"Simulation started. Calling external service... | 1")
    wsClient.url(externalUrl)
      .withHttpHeaders(("X-Correlation-ID", MDC.get("correlationId")))
      .post("")
      .flatMap( r => {
        Future {
          log.info(s"Nested future. Sleep 1000 ms. | 2")
          Thread.sleep(1000)
          log.info(s"Nested future. Wake up. | 3")
        }
      })

    Future {
      log.info(s"Future with custom context. Sleep 1000 ms. | 4")
      Thread.sleep(1000)
      log.info(s"Future with custom context. Wake up. | 5")
    }(customEc)

    Future {
      log.info(s"Future with default context. Sleep 1000 ms. | 6")
      Thread.sleep(1000)
      log.info(s"Future with default context. Wake up. | 7")
    }
  }

}

object CorrelationIdPocSimulator {
  private val log = play.api.Logger(getClass)
}
