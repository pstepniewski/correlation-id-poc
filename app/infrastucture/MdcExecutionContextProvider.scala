package infrastucture

import javax.inject.{Inject, Provider, Singleton}
import org.slf4j.MDC

import scala.concurrent.ExecutionContext

@Singleton
class MdcExecutionContextProvider @Inject()(delegate: ExecutionContext) extends Provider[MdcExecutionContext]{

  override def get: MdcExecutionContext = new MdcExecutionContext(MDC.getCopyOfContextMap, delegate)

  object Implicits {
    implicit def get: MdcExecutionContext = new MdcExecutionContext(MDC.getCopyOfContextMap, delegate)
  }
}
